# Technical Documentation

1. [Make your own levels](#own-levels)
2. [Notes on the server](#server-notes)
3. [Notes on the client](#client-notes)
4. [Troubleshooting](#troubleshooting)
5. [Network port configuration](#port-conf)

## Make your own levels <a name="own-levels"></a>

Paxed has written an online level editor, but it is down nowadays. You 
could build your levels on this web page, and save them locally in order 
to play them. The level files should be stored in the bubbob/levels 
subdirectory.

If you are interested in editing the levels locally with a text editor, 
read on. Level files come in two flavours: .bin and .py. The former 
are MacBinary files from the [original MacOS9 game](http://www.mcsebi.com/). 
The latter are the 
levels that we did ourselves. To make new levels directly with a text 
editor, copy for example CompactLevels.py to a new name, and open it 
with any text editor. The structure should be fairly simple to 
understand even without knowledge about Python. Just don't try to 
look at RandomLevels.py first :-)

---------------------

## Notes on the server <a name="server-notes"></a>

The server is the single program that computes all the game for all 
the players, but doesn't display anything. It should be run on the 
fastest machine, or even better on another machine of your local 
network where no one is playing (although modern computers don't 
have any trouble running both the server and a client).

The server can be run in the command-line only; `try cd bubbob and 
python bb.py --help.`

---------------------

## Notes on the client <a name="client-notes"></a>

If you want to understand a bit more what's going on with all these 
different ways to run the client (display) part, read on. The client 
will run in any one of the following -- make sure you do have one of 
these, in order of preference: 

* Pygame. This is a cool gaming library for Python. It is available for 
  quite a lot of platforms. (Also see the kitchensink link above for 
  MacOS X.) This gives the nicer results, so if you can install it, go 
  ahead! You'll get transparency effects on the bubbles!

  or

* MS Windows. (It means that Bub & Bros runs natively on Windows; but 
  you can also install Pygame on top of Windows for the nicer bubbles :-)

  or

* The X Window system (the graphical environment of Unix/Linux). The 
  native X Window system is pretty fast under Bub & Bros, so try this 
  if you have a slow Unix/Linux machine. Note that you could also try 
  it on MacOSX's X11 (I guess you need to start display/Client.py from 
  an xterm command-line for this to work).

  or

* GTK (the Gnome environment) if the PyGTK bindings are installed. 
  Unlike the other drivers this one offers you the ability to scale 
  the window, zooming its content in or out. Use it if your screen has 
  a specially large or small resolution.

  or

* A Java-enabled web browser. No installation required. (But you won't 
  have the nice background musics when playing in the browser, so I'd 
  recommend that you try one of the solutions above as well, to really 
  enjoy the game.)

The client can also be run from the command-line; try `cd display` 
and `python Client.py --help`.

---------------------

## Troubleshooting <a name="troubleshooting"></a>

* If, before you even try to join a game, you are stuck at some point 
  (where nothing occurs, no window opens,...) then you may be able to 
  have a hint at what is going wrong by looking near the end of the 
  log file, which is created under the name BubBob.log besides BubBob.py, 
  or under the name bb.log in the Windows Temp directory. If you can't 
  make sense of it feel free to send it to us (address below).
* Firewalls and NAT systems are the most common cause for preventing 
  connexions with Internet servers. NAT systems are routers, common for 
  example in ADSL setups, that allow a sub-network to use private IP 
  addresses like 192.168.1.2. The various programs (client, server, 
  meta-server) are doing their very best to connect to each other 
  nonetheless. If you still cannot connect to a server, see next section. 

---------------------

## Network port configuration <a name="port-conf"></a>

**First note** that if a local connexion doesn't work, you should try using 
the meta-server:

* When you start the server, check "register to the meta-server".
* On the client, from BubBob's main page, go to the "Internet servers page". 

If it doesn't help, and if you're technically inclined, read on.

**Local connexions**: if you cannot see a server in the list of local 
servers, then:

* Either the server is not local (i.e. not close enough, network-wise, 
  from your machine). In this case enter the full name of the server 
  in the box below (it should look like `machinename.domain.com:1234`, 
  as displayed on the server when it starts).
* Or the server is not listening on its UDP port 8056, meaning that this 
  port was already in use (e.g. if you run several Bub's Brothers servers 
  on the same machine, only one of them will show up); clients then have 
  to enter the full name of the server as above.
* Or there is a firewall between you and the server. Try connecting 
  through the meta-server. Alternatively, read on. 

During a normal connexion (not going through the meta-server) the client 
needs to open a TCP connexion to the server. The server can be configured 
to listen to a specific port ("TCP server port" of the configuration page) 
if you can let this port in through a firewall or router.

Servers listen to HTTP as well. This is used for both the local BubBob 
pages (if you have a very restrictive firewall on your own machine then 
you might not be able to connect to your own machine, unless you let that 
port through) and for remote clients to which the server sends a Java 
applet. You can force a port in "HTTP server port". It default to 8000 
if this port is free.

The server sends UDP datagrams to the clients during the game; if these 
ones don't pass through, they are re-routed over TCP, so it should just 
work. Nevertheless you might prefer UDP for responsiveness. The client 
can choose on which UDP port it will listen for incoming datagrams ("client 
incoming UDP port" of the configuration page) if you can let this port 
in through the firewall.

If a client has no way to establish a direct connexion but has an ssh 
account on a machine near the server, he can do:
`ssh somewhere.domain.org -L1234:servername:4321`
where servername:4321 is the full name of the server, and 1234 is some 
randomly choosen local port. Then you can connect BubBob to 127.0.0.1:1234 
using the Go button on the main page. In this setting, if you can still 
let UDP datagrams enter, then configure "client incoming UDP port" to be 
not only the port but the whole name of the machine (or router) that the 
server must send the datagrams to.

**Meta-server connexions**: Connexions using the meta-server (with the 
"Internet server page" from BubBob's main page) work generally better 
than direct connexions, as long as the server and the client can 
independently connect to the meta-server (which is at ctpug.org.za 
using TCP ports 8050 and 8055). Firewalls and routers generally allow this. 
The meta-server will then co-operate with the server and the client 
to synchronize their connexion. (The meta-server will not route game 
data between the server and the client, though.)

This doesn't always work either, though. If it does not, then you need 
to open a port on your side. You have to configure your firewall and/or
your NAT (e.g. your ADSL modem) to allow an incoming port. In the NAT 
case, you have to say that communication arriving from outside at a 
TCP port of your choice (pick a number, say, between 8000 and 9999) 
should be sent to your computer (you have to enter its IP, as seen in 
the "network connexions" configuration). You can then tell bub-n-bros 
to use this port: click on the Configuration link on the main page, 
scroll down to Network options, and enter the same port number in 
"Client incoming TCP port").

More technically, with the help of the meta-server, the games tries the 
following (in order):

1. The client tries to connect to the server normally, as in local connexions. 
   It fails if the server cannot be reached through a firewall or router. 
   To fix this, the person starting the server can open a TCP port and 
   enter it in "TCP game server port" in the Configuration page.
2. The client (through the meta-server) asks the server to try to connect 
   backwards to the client. This fails if the client cannot be reached 
   through a firewall or router. On the client, use "Client incoming TCP 
   port" to fix a specific port that you can let in, as described above.
3. They try to connect to each other using a technique known as simultaneous 
   SYN exchanges. In some cases it works even if both the client and 
   the server are behind a firewall. This is not a way to hack through 
   firewalls and break the security of a network, it is a perfectly 
   official (if rarely used) way to connect two machines and it only 
   works if the two machines are willing to actively connect to each 
   other in the first place. 

---------------------

You can issue any question or trouble on gitlab 
[gitlab.com/solebull/solebub](https://gitlab.com/solebull/solebub/-/issues).
