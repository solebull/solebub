# solebub

A new striking n-multiplayer crazy clone of the famous Bubble Bobble game.
This is a fork of The Bub's Brothers By Armin Rigo & IMA connection
(http://bub-n-bros.sourceforge.net/).

## Dependencies

Please install these packages, at least on debian GNU/linux (More to come) :

	sudo apt install python3 python3-pygame libpython2.7-dev gcc groff markdown

## Building

	make
	
## Running

The following command :

	python BubBob.py

will automatically open a local website (http://127.0.0.1:8000/0xbdda/).
Then click on *Start a new game*, configure and start it.

Now, you can follow the **Join your own game now** link, it will open a new
game window.

## Command line arguments

from the `python bubbob/bb.py --help` command :

	usage:
	  python bb.py
	or:
	  python bb.py [level-file.bin] [-m] [-b#] [-s#] [-l#] [-M#]
	with options:
	  -m  --metaserver  register the server on the Metaserver so anyone can join
	  -b#  --begin #    start at board number # (default 1)
	       --start #    synonym for --begin
	       --final #    end at board number # (default 100)
	  -s#  --step #     advance board number by steps of # (default 1)
	  -l#  --lives #    limit the number of lives to #
	       --extralife #    gain extra life every # points
	       --limitlives #    max # of lives player can gain in one board
	  -M#  --monsters # multiply the number of monsters by #
                      (default between 1.0 and 2.0 depending on # of players)
	  -i   --infinite   restart the server at the end of the game
	  --port LISTEN=#   set fixed tcp port for game server
	  --port HTTP=#     set fixed tcp port for http server
	  -h   --help       display this text

## Playing

### Zoom handling

Only the *pygame* powered version handles a zoom feature.

### Pause

Simply remove all players. You'll stay on the actual level.

### Procedural/random levels

When starting a new game, choose **RandomLevels** from the *Level file*
dropdown menu.

## Troubleshooting


If the game doesn't play sounds/music, please be sure you installed the
`python-pygame` package (especilly the pygame mixer module) and restart 
webserver.
